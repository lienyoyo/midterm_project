# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : [Shopping Palace]
* Key functions (add/delete)
    1. [Shopping website]
    
* Other functions (add/delete)
    1. [商品篩選]
    2. [類型新增]
    3. [個人頁面]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：[https://midterm-project-44caf.firebaseapp.com]

# Components Description : 
1. [Membership Mechanism] : [可以登入、註冊新用戶，並產生用戶資訊]
2. [Topic Key Function] : [可在商品列表查看商品，點擊可查看詳情(名稱、價格、賣家)，並可在登入後下訂單。買家與賣家可在dashboard看到購買紀錄、販賣的商品和收到的訂單(點擊可查看詳情)，以及可以在我的賣場中點選添加新商品]
3. [Chrome Notification] : [在商品列表可以開啟通知，新商品上架時，會收到通知]


# Other Functions Description(1~10%) : 
1. [商品篩選] : [可以在商品列表左邊選擇類型來篩選商品]
2. [類型新增] : [可以在商品新增頁面中動態添加新類型]
3. [個人頁面] : [可以在setting中修改個人資料(暱稱、頭貼、自我介紹等等)，並可以在商品資訊、購物紀錄中查看賣家資訊；在訂單資訊查看買家資訊]


## Security Report (Optional)
在商品描述或自我介紹中加入HTML語法並不會產生效果，只會是普通文字。
ex:< script>alert("test");</ script>